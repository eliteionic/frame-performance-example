import { Component, OnInit } from '@angular/core';
import { AnimationController, Animation } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  public items = [];

  constructor(private animationCtrl: AnimationController) {}

  ngOnInit() {
    this.items = [
      { id: 1, title: 'One' },
      { id: 2, title: 'Two' },
      { id: 3, title: 'Three' },
      { id: 4, title: 'Four' },
      { id: 5, title: 'Five' },
      { id: 6, title: 'Six' },
      { id: 7, title: 'Seven' },
      { id: 8, title: 'Eight' },
      { id: 9, title: 'Nine' },
      { id: 10, title: 'Ten' },
      { id: 11, title: 'Eleven' },
      { id: 12, title: 'Twelve' },
      { id: 13, title: 'Thirteen' },
      { id: 14, title: 'Fourteen' },
      { id: 15, title: 'Fifteen' },
      { id: 16, title: 'Sixteen' },
      { id: 17, title: 'Seventeen' },
      { id: 18, title: 'Eighteen' },
      { id: 19, title: 'Nineteen' },
      { id: 20, title: 'Twenty' },
    ];
  }

  async deleteCard(target, itemToDelete) {
    const cardToAnimate = target.closest('ion-card');

    // This will perform poorly/cause layouts
    // const animation: Animation = this.animationCtrl
    //   .create()
    //   .addElement(cardToAnimate)
    //   .duration(200)
    //   .easing('ease-in')
    //   .fromTo('opacity', '1', '0')
    //   .fromTo('height', '120px', '0px');

    // This will perform well (no layouts)
    const animation: Animation = this.animationCtrl
      .create()
      .addElement(cardToAnimate)
      .duration(500)
      .easing('ease-out')
      .fromTo('opacity', '1', '0');

    await animation.play();

    this.items = [...this.items.filter((item) => item.id !== itemToDelete.id)];
  }
}
